/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import BO.*;
import DAO.*;
/**
 *
 * @author AlexisENP
 */
public class ClientesCtrl {
    public List<clientesBO> buscarCliente(Object obj) throws ParseException{
	ClienteQueryHlp oClienteQueryHlp=new ClienteQueryHlp();
	List<clientesBO> oClientesBO=new ArrayList<clientesBO>();
         for(clientesBO p:oClienteQueryHlp.buscar(obj))
         {			
            clientesBO oClientes=new clientesBO();
            oClientes.setIdClientes(p.getIdClientes());
            oClientes.setNombre(p.getNombre());
            oClientes.setApellidosP(p.getApellidosP());
            oClientes.setApellidosM(p.getApellidosM());
            oClientes.setTelefono(p.getTelefono());
            oClientes.setDireccion(p.getDireccion());
            oClientes.setMail(p.getMail());
            oClientes.setCredito(p.getCredito());
             oClientes.setDeuda(p.getDeuda());
            oClientesBO.add(oClientes);
         }
	return oClientesBO;
    } 
}
