/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;
import BO.*;
import DAO.VentaHlpr;
import java.sql.SQLException;
import java.util.ArrayList;
/**
 *
 * @author Jaime
 */
public class VentaService {
 
    public ProductoBO buscarProducto(int codigo) throws SQLException{
        VentaHlpr venta = new VentaHlpr();
        return venta.buscarProducto(codigo);
    }
    
    public int generarVenta(VentaBO venta){
        return new VentaHlpr().generarVenta(venta);
    }

    public ArrayList buscarProductos(ProductoBO productoBO) {
        VentaHlpr venta = new VentaHlpr();
        return venta.buscarProductos(productoBO);
    }
    
     public ArrayList traerCLientes() {
        VentaHlpr venta = new VentaHlpr();
        return venta.traerClientes();
    }
     
     public ArrayList traerVentas() {
        VentaHlpr venta = new VentaHlpr();
        return venta.traerVentas();
    
     }
     
      public ClienteBO trearCliente(int idCliente) {
        VentaHlpr venta = new VentaHlpr();
        return venta.traerCliente(idCliente);
    }
    
}
