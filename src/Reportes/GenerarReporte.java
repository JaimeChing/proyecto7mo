/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reportes;


import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;





/**
 *
 * @author diegoalexis
 */
public class GenerarReporte {
   
    
    public void reporteUsuario() throws JRException, SQLException, IOException{
       // try {
            JasperReport report;
            report = (JasperReport) JRLoader.loadObjectFromFile("ClienteReporte.jasper");
            Fabrica.ConexionMySQL a= new Fabrica.ConexionMySQL();
            Connection conexion = a.getConexion();
            JasperPrint j = JasperFillManager.fillReport(report,null,conexion);
            JasperViewer jv = new JasperViewer(j,false);
            jv.setTitle("reporte usuarios");
            jv.setVisible(true);
        /*} catch (Exception e) {
            JOptionPane.showMessageDialog(null,"error al mostrar el reporte "+e);
        }*/
    }
    
}
