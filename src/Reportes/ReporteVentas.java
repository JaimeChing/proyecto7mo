/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reportes;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;


/**
 *
 * @author pc
 */
public class ReporteVentas {
    
    public void ReporteVenta() throws SQLException,JRException, ClassNotFoundException, IOException{
        Fabrica.ConexionMySQL a= new Fabrica.ConexionMySQL();
        Class.forName("com.mysql.jdbc.Driver"); 
        Connection conexion = a.getConexion();
        JasperReport report;
        String helper = new File(".").getCanonicalPath();
        report = (JasperReport) JRLoader.loadObjectFromFile(helper  + "\\src\\Reportes\\ReporteVentas.jasper");
        JasperPrint im=JasperFillManager.fillReport(report,null,conexion);
        JasperViewer ver=new JasperViewer(im,false);
        ver.setTitle("Ticket de venta");
        ver.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); 
        ver.setVisible(true);
        
    }
    public void ReporteVenta(int idVenta) throws SQLException,JRException, ClassNotFoundException, IOException{
        Map parametros;
        parametros = new HashMap<>();
        parametros.put("IdVenta", idVenta);
        Fabrica.ConexionMySQL a= new Fabrica.ConexionMySQL();
        Class.forName("com.mysql.jdbc.Driver"); 
        Connection conexion = a.getConexion();
        JasperReport report;
        //report = (JasperReport) JRLoader.loadObjectFromFile("C:\\Users\\pc\\Documents\\NetBeansProjects\\proyecto7mo\\src\\Reportes\\ReporteVentas_1.jasper")
        JasperPrint im;
        String helper = new File(".").getCanonicalPath();
        im = JasperFillManager.fillReport(helper  + "\\src\\Reportes\\ReporteVentas_1.jasper",parametros, conexion);
        JasperViewer ver=new JasperViewer(im,false);
        ver.setTitle("Ticket de venta");
        ver.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); 
        ver.setVisible(true);
        
    }
}
