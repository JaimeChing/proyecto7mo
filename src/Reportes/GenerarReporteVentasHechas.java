/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reportes;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;
/**
 *
 * @author pc
 */
public class GenerarReporteVentasHechas {
    public void ReporteVentasHechas(String fechaA,String fechaB) throws SQLException,JRException, ClassNotFoundException, IOException, ParseException{
        Map parametros;
        parametros = new HashMap();
        parametros.put("Fecha1", fechaA);
        parametros.put("Fecha2", fechaB);
        Fabrica.ConexionMySQL a= new Fabrica.ConexionMySQL();
        Class.forName("com.mysql.jdbc.Driver"); 
        Connection conexion = a.getConexion();
        JasperReport report;
        //report = (JasperReport) JRLoader.loadObjectFromFile("C:\\Users\\pc\\Documents\\NetBeansProjects\\proyecto7mo\\src\\Reportes\\ReporteVentas_1.jasper")
        JasperPrint im;
        String helper = new File(".").getCanonicalPath();
        im = JasperFillManager.fillReport(helper  + "\\src\\Reportes\\ReporteVentasHechas.jasper",parametros, conexion);
        JasperViewer ver=new JasperViewer(im,false);
        ver.setTitle("Ventas en fechas"+fechaA+" - "+fechaB);
        ver.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); 
        ver.setVisible(true);
    }
}
