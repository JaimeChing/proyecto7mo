/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;
import BO.ClienteBO;
import BO.ProductoBO;
import DAO.VentaHlpr;
import Services.VentaService;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Jaime
 */
public class PuntoVentaGUI extends javax.swing.JFrame {

    /**
     * Creates new form PuntoVenta
     */
     public static GUI.BusquedaPersonalizadaGUI n;
     public static int[] idCliente;

     
    public PuntoVentaGUI() {
        initComponents();
        //Llenar combo
        llenarComboClientes();
        
        init();
        
    }
    
    void init(){
         try{
             txtInvisible.setVisible(false);
             tblProducts.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
                 public void valueChanged(ListSelectionEvent event) {
                     if(tblProducts.getSelectedRow() >= 0){
                         btnRemove.setEnabled(true);
                     }else{
                         btnRemove.setEnabled(false);
                     }
                 }
             });
             
             DefaultTableModel modelProductos = new DefaultTableModel();
             modelProductos.addColumn("Cantidad");
             modelProductos.addColumn("Producto");
             modelProductos.addColumn("Precio");
             modelProductos.addColumn("Subtotal");
             modelProductos.addColumn("Subtotal Credito");
             modelProductos.addColumn("IdProducto");

             tblProducts.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
             tblProducts.getTableHeader().setReorderingAllowed(true);
             TableRowSorter<TableModel> ordena = new TableRowSorter<TableModel>(modelProductos);
             tblProducts.setRowSorter(ordena);
             tblProducts.setModel(modelProductos);
             tblProducts.getColumnModel().getColumn(0).setPreferredWidth(20);
             tblProducts.getColumnModel().getColumn(1).setPreferredWidth(130);
             tblProducts.getColumnModel().getColumn(2).setPreferredWidth(130);
             tblProducts.getColumnModel().getColumn(3).setPreferredWidth(130);
             tblProducts.getColumnModel().getColumn(4).setPreferredWidth(130);

             tblProducts.getColumnModel().getColumn(5).setPreferredWidth(0);
             tblProducts.getColumnModel().getColumn(5).setMaxWidth(0);
             tblProducts.getColumnModel().getColumn(5).setMinWidth(0);
         }
         catch(Exception ex){}
                
        
    }
    
    public void ActionFacade(String action) throws ParseException, SQLException{
        switch(action){
            case "FINDPRODUCT":
                if(txtClave.getText().length() ==0){
                    JOptionPane.showMessageDialog(null, "Debes ingresar la clave de un producto primero y luego buscar");
                    return;
                }
                ArrayList Productos = new VentaService().buscarProductos(new BO.ProductoBO(0, "".equals(txtClave.getText()) ? null: txtClave.getText() , "", null,0,0,0,0));
                
                if(Productos.isEmpty()){
                    JOptionPane.showMessageDialog(null, "No se encontró ningún producto con esa clave.");
                    return;
                }
                ProductoBO productoResultante = (ProductoBO)Productos.get(0);
                txtProducto.setText(productoResultante.getNombre());
                txtPrecio.setText(Double.toString(productoResultante.getPrecio()));
                txtCredito.setText(Double.toString(productoResultante.getPrecioCredito()));
                txtInvisible.setText(Integer.toString(productoResultante.getIdProducto()));
                break;
            case "OPENBP":
                n  =  new GUI.BusquedaPersonalizadaGUI(new javax.swing.JDialog(), true);
                n.addComponentListener(new ComponentAdapter() {
                    public void componentHidden(ComponentEvent e) {
                        String[] fila = new String[5];
                        ProductoBO productoResultante = n.getProductoResultante();
                        try {
                            productoResultante = new VentaService().buscarProducto(productoResultante.getIdProducto());
                        } catch (SQLException ex) {
                            Logger.getLogger(PuntoVentaGUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        System.out.println(productoResultante.getNombre());
                        txtProducto.setText(productoResultante.getNombre());
                        txtPrecio.setText(Double.toString(productoResultante.getPrecio()));
                        txtCredito.setText(Double.toString(productoResultante.getPrecioCredito()));
                        txtInvisible.setText(Integer.toString(productoResultante.getIdProducto()));
                    }

                   
                });
                n.setVisible(true);
                break;
            case "ADDPRODUCT":
                if(txtProducto.getText().length() == 0){
                    JOptionPane.showMessageDialog(null, "Debes buscar un producto primero y luego ingresar una cantidad");
                    return;
                }
                if(txtCantidad.getText().length() > 0){
                    
                    String[] fila = new String[6];
                    fila[0] = txtCantidad.getText();
                    fila[1] = txtProducto.getText();
                    fila[2] = txtPrecio.getText();
                    fila[3] = Double.toString(Double.parseDouble(txtCantidad.getText() )  * Double.parseDouble(txtPrecio.getText()));
                    fila[5] = txtInvisible.getText();
                    fila[4] = Double.toString(Double.parseDouble(txtCantidad.getText() )  * Double.parseDouble(txtCredito.getText()));

                    
                    
                    DefaultTableModel helper =  (DefaultTableModel)tblProducts.getModel();
                    helper.addRow(fila);
                    tblProducts.setModel(helper);
                    
                    Double total;
                    
                    if("".equals(txtTotal.getText())){
                        total = 0.0;
                    }else{
                        total = Double.parseDouble(txtTotal.getText());
                    }
                    total = Double.parseDouble(fila[3]) + total;
                    txtTotal.setText(Double.toString(total));
                    
                                        
                    if("".equals(txtTotalCredito.getText())){
                        total = 0.0;
                    }else{
                        total = Double.parseDouble(txtTotalCredito.getText());
                    }
                    total = Double.parseDouble(fila[4]) + total;
                    txtTotalCredito.setText(Double.toString(total));
                    Clear();
                }else{
                    JOptionPane.showMessageDialog(null, "Debes ingresar una cantidad");
                    return;
                }
                break;
            case "ADDVENTA":
                DefaultTableModel hlpr =  (DefaultTableModel)tblProducts.getModel();
               
                int count = hlpr.getRowCount();
                
                if(count == 0){
                    JOptionPane.showMessageDialog(null, "Aún no hay productos para iniciar una venta");
                    return;
                }
                if(cbClientes.getSelectedIndex() == -1){
                                    
                    JOptionPane.showMessageDialog(null, "Aún no hay un cliente seleccionado para iniciar una venta");
                    return;
                }
                
                int[] ids = new int[count];
                ArrayList productos = new  ArrayList();
                for(int i = 0; i < count; i++){
                    int id = Integer.parseInt(hlpr.getValueAt(i, 5).toString());
                    int cantidad = Integer.parseInt(hlpr.getValueAt(i, 0).toString());
                    ProductoBO helper = new VentaService().buscarProducto(id);
                    if(helper.getStock() < cantidad ){
                       JOptionPane.showMessageDialog(null, "No se dispone de la cantidad correcta para vender el producto: " + helper.getNombre() + " en el sistema solo quedan: " + helper.getStock());
                       return;
                    }
                    helper.setCantidadAVender(cantidad);
                    productos.add(helper);
                }
                generarventa(idCliente[cbClientes.getSelectedIndex()], productos);
                break;
            case "REMOVEPRODUCT":
                int i = tblProducts.getSelectedRow();
                DefaultTableModel helper =  (DefaultTableModel)tblProducts.getModel();
                String o = helper.getValueAt(i, 3).toString();
                Double total = Double.parseDouble(o);
                Double totalHelper = Double.parseDouble(txtTotal.getText());
                total = totalHelper - total;
                txtTotal.setText(total.toString());
                
                o = helper.getValueAt(i, 4).toString();
                total = Double.parseDouble(o);
                totalHelper = Double.parseDouble(txtTotalCredito.getText());
                total = totalHelper - total;
                txtTotalCredito.setText(total.toString());
                
                helper.removeRow(i);
                tblProducts.setModel(helper);
                break;
        }
    }
    
    void generarventa( int idCliente, ArrayList productos){
        PagoGUI opcionPago = new PagoGUI(new javax.swing.JDialog(), true, idCliente, txtTotal.getText(), txtTotalCredito.getText(), productos);
        opcionPago.addComponentListener(new ComponentAdapter() {
            public void componentHidden(ComponentEvent e) {
                Clear();
                ClearTotal();
                ClearTable();
            }
                
     
            
        });
        opcionPago.setVisible(true);
        
    }
    
    private void ClearTable() {
        DefaultTableModel helper = (DefaultTableModel)tblProducts.getModel();
        int count = helper.getRowCount();
        for (int i = count - 1; i > -1; i--) {
            helper.removeRow(i);
        }
        tblProducts.setModel(helper);
    }
    
    
    void Clear(){
        txtCantidad.setText("");     
        txtProducto.setText("");
        txtPrecio.setText("");
        txtClave.setText("");
        txtCredito.setText("");
    }
    
    void ClearTotal(){
        txtTotal.setText("0");
        txtTotalCredito.setText("0");
    }
    
    void llenarComboClientes(){
        cbClientes.removeAllItems();
        ArrayList n = new VentaService().traerCLientes();
        idCliente = new int[n.size()];

        for(int i =0; i< n.size(); i++){
            System.out.print(((ClienteBO)n.get(i)).getId_cliente());
            idCliente[i] = ((ClienteBO)n.get(i)).getId_cliente();
            cbClientes.addItem(((ClienteBO)n.get(i)).getNombres() + " " + ((ClienteBO)n.get(i)).getApellido_Paterno() + " " + ((ClienteBO)n.get(i)).getApellido_Materno());
        }
    }
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        txtProducto = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtPrecio = new javax.swing.JTextField();
        btnAddProduct = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblProducts = new javax.swing.JTable();
        txtTotal = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtClave = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();
        btnConfirmar = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        txtCantidad = new javax.swing.JTextField();
        btnPersonalizada = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        cbClientes = new javax.swing.JComboBox<>();
        btnClear = new javax.swing.JButton();
        btnRemove = new javax.swing.JButton();
        txtInvisible = new javax.swing.JTextField();
        btnVentas = new javax.swing.JButton();
        txtCredito = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtTotalCredito = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        btnClientes = new javax.swing.JButton();

        jButton1.setText("jButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Producto:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Punto de venta");

        jButton2.setText("Búsqueda personalizada...");
        jButton2.setName("btnBusquedaPer"); // NOI18N

        txtProducto.setEditable(false);
        txtProducto.setName("txtProductName"); // NOI18N

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Clave:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setText("Precio:");

        txtPrecio.setEditable(false);
        txtPrecio.setToolTipText("");
        txtPrecio.setName("txtPrecio"); // NOI18N
        txtPrecio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPrecioActionPerformed(evt);
            }
        });

        btnAddProduct.setText("Agregar");
        btnAddProduct.setToolTipText("Si el producto que encontraste es correcto, ingresa una cantidad y luego presiona este botón");
        btnAddProduct.setActionCommand("ADDPRODUCT");
        btnAddProduct.setName("btnAgregar"); // NOI18N
        btnAddProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddProductActionPerformed(evt);
            }
        });

        tblProducts.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblProducts.setName("tblProductos"); // NOI18N
        jScrollPane1.setViewportView(tblProducts);

        txtTotal.setEditable(false);
        txtTotal.setText("0");
        txtTotal.setName("txtTotal"); // NOI18N

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setText("Total:");

        txtClave.setName("txtClave"); // NOI18N

        btnBuscar.setText("Buscar");
        btnBuscar.setToolTipText("Una vez ingresada la clave, presiona el botón");
        btnBuscar.setActionCommand("FINDPRODUCT");
        btnBuscar.setName("btnBuscar"); // NOI18N
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnConfirmar.setText("Confirmar venta");
        btnConfirmar.setToolTipText("Confirma la venta, presionando este botón");
        btnConfirmar.setActionCommand("ADDVENTA");
        btnConfirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmarActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setText("Cantidad:");

        btnPersonalizada.setText("Búsqueda personalizada");
        btnPersonalizada.setActionCommand("OPENBP");
        btnPersonalizada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPersonalizadaActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel7.setText("Cliente:");

        cbClientes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        btnClear.setText("Limpiar");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnRemove.setText("Remover producto");
        btnRemove.setActionCommand("REMOVEPRODUCT");
        btnRemove.setEnabled(false);
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });

        txtInvisible.setEditable(false);
        txtInvisible.setEnabled(false);

        btnVentas.setText("Módulo ventas");
        btnVentas.setToolTipText("Confirma la venta, presionando este botón");
        btnVentas.setActionCommand("ADDVENTA");
        btnVentas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVentasActionPerformed(evt);
            }
        });

        txtCredito.setEditable(false);

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel8.setText("Efectivo");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setText("Crédito");

        txtTotalCredito.setEditable(false);
        txtTotalCredito.setText("0");
        txtTotalCredito.setName("txtTotal"); // NOI18N

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel10.setText("Efectivo");

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel11.setText("Crédito");

        btnClientes.setText("Clientes");
        btnClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClientesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtInvisible, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnClientes)
                                .addGap(18, 18, 18)
                                .addComponent(btnVentas, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnRemove, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(btnClear)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnConfirmar, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel10))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel11)
                                            .addComponent(txtTotalCredito, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 766, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel3)
                                            .addGap(38, 38, 38)
                                            .addComponent(txtClave, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel6)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(18, 18, 18)
                                            .addComponent(jLabel1)))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(btnBuscar)
                                            .addGap(18, 18, 18)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(btnPersonalizada, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(txtProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(18, 18, 18)
                                            .addComponent(jLabel4)))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel7)
                                            .addGap(18, 18, 18)
                                            .addComponent(cbClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(txtPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabel8))
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel9)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(txtCredito, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGap(26, 26, 26)
                                                    .addComponent(btnAddProduct)))))))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(352, 352, 352)
                        .addComponent(jLabel2)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(txtClave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar)
                    .addComponent(btnPersonalizada)
                    .addComponent(jLabel7)
                    .addComponent(cbClientes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9))
                .addGap(1, 1, 1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtCredito, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4)
                        .addComponent(btnAddProduct)
                        .addComponent(jLabel6)
                        .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(txtTotalCredito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnConfirmar)
                    .addComponent(btnClear)
                    .addComponent(btnRemove)
                    .addComponent(txtInvisible, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnVentas)
                    .addComponent(btnClientes))
                .addGap(30, 30, 30))
        );

        txtClave.getAccessibleContext().setAccessibleName("txtClave");
        btnBuscar.getAccessibleContext().setAccessibleName("btnBuscar");
        btnConfirmar.getAccessibleContext().setAccessibleName("txtConfirmar");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnPersonalizadaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPersonalizadaActionPerformed
       
        try {
            try {
                ActionFacade(evt.getActionCommand().toString());
            } catch (SQLException ex) {
                Logger.getLogger(PuntoVentaGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ParseException ex) {
            Logger.getLogger(PuntoVentaGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnPersonalizadaActionPerformed

    private void txtPrecioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPrecioActionPerformed
     
    }//GEN-LAST:event_txtPrecioActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
         try {
            ActionFacade(evt.getActionCommand().toString());
        } catch (ParseException ex) {
            Logger.getLogger(PuntoVentaGUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
             Logger.getLogger(PuntoVentaGUI.class.getName()).log(Level.SEVERE, null, ex);
         }
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnAddProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddProductActionPerformed
        try {
            ActionFacade(evt.getActionCommand().toString());
        } catch (ParseException ex) {
            Logger.getLogger(PuntoVentaGUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
             Logger.getLogger(PuntoVentaGUI.class.getName()).log(Level.SEVERE, null, ex);
         }
    }//GEN-LAST:event_btnAddProductActionPerformed

    private void btnConfirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmarActionPerformed
         try {
            ActionFacade(evt.getActionCommand().toString());
        } catch (ParseException ex) {
            Logger.getLogger(PuntoVentaGUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
             Logger.getLogger(PuntoVentaGUI.class.getName()).log(Level.SEVERE, null, ex);
         }
    }//GEN-LAST:event_btnConfirmarActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        Clear();
        ClearTable();
        ClearTotal();
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
         try {
            ActionFacade(evt.getActionCommand().toString());
        } catch (ParseException ex) {
            Logger.getLogger(PuntoVentaGUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
             Logger.getLogger(PuntoVentaGUI.class.getName()).log(Level.SEVERE, null, ex);
         }
    }//GEN-LAST:event_btnRemoveActionPerformed

    private void btnVentasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVentasActionPerformed
        VentasGUI ventanaVentas = new  VentasGUI();
        ventanaVentas.setVisible(true);
        
    }//GEN-LAST:event_btnVentasActionPerformed

    private void btnClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClientesActionPerformed
        // TODO add your handling code here:
        consultaClientes clientes = new consultaClientes();
        clientes.setVisible(true);
    }//GEN-LAST:event_btnClientesActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PuntoVentaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PuntoVentaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PuntoVentaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PuntoVentaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PuntoVentaGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddProduct;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnClientes;
    private javax.swing.JButton btnConfirmar;
    private javax.swing.JButton btnPersonalizada;
    private javax.swing.JButton btnRemove;
    private javax.swing.JButton btnVentas;
    private javax.swing.JComboBox<String> cbClientes;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblProducts;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtClave;
    private javax.swing.JTextField txtCredito;
    private javax.swing.JTextField txtInvisible;
    private javax.swing.JTextField txtPrecio;
    private javax.swing.JTextField txtProducto;
    private javax.swing.JTextField txtTotal;
    private javax.swing.JTextField txtTotalCredito;
    // End of variables declaration//GEN-END:variables
}
