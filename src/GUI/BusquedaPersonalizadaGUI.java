/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import BO.ProductoBO;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Console;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Jaime
 */
public class BusquedaPersonalizadaGUI extends javax.swing.JDialog {

    /**
     * Creates new form BusquedaPersonalizada
     */
    
    ProductoBO productoResultante = new ProductoBO();

    public ProductoBO getProductoResultante() {
        return productoResultante;
    }
    
    
    public BusquedaPersonalizadaGUI(javax.swing.JDialog parent, boolean modal) {
        super(parent,modal);
        initComponents();   
        
        tblProductos.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
        public void valueChanged(ListSelectionEvent event) {
            // do some actions here, for example
            // print first column value from selected row
            if(tblProductos.getSelectedRow() >= 0){
                btnAgregar.setEnabled(true);
            }else{
                btnAgregar.setEnabled(false);
            }
        }
        });
        init();
    }
    

    public void ActionFacade(String action) throws ParseException{
        switch(action){
            case "FIND":
                try{
                    Services.VentaService n = new Services.VentaService();
                    if("".equals(txtClave.getText()) && "".equals(txtNombre.getText())){
                        init();
                    }else{
                        ArrayList productos = n.buscarProductos(new BO.ProductoBO(0, "".equals(txtClave.getText()) ? null: txtClave.getText() , "".equals(txtNombre.getText()) ? null: txtNombre.getText(), null,0,0,0,0));                   
                        
                        Object[] fila = new Object[5];
                        DefaultTableModel modelProductos = new DefaultTableModel();
                        modelProductos.addColumn("Id");
                        modelProductos.addColumn("Nombre");
                        modelProductos.addColumn("Precio");
                        modelProductos.addColumn("Precio a crédito");
                        modelProductos.addColumn("Clave");
                        
                        for(int i = 0; i < productos.size(); i ++){
                            
                            fila[0] = Integer.toString(((ProductoBO)productos.get(i)).getIdProducto());
                            fila[1] = ((ProductoBO)productos.get(i)).getNombre();
                            fila[2] = Double.toString(((ProductoBO)productos.get(i)).getPrecio());
                            fila[3] = Double.toString(((ProductoBO)productos.get(i)).getPrecioCredito());
                            fila[4] = ((ProductoBO)productos.get(i)).getNombreClave();
                            JButton boton = new JButton("Agregar");
                            boton.setSize(100,45);
                            boton.setName(Integer.toString(((ProductoBO)productos.get(i)).getIdProducto()));
                            boton.setVisible(true);
                            ActionListener listener = new ActionListener(){
                                public void actionPerformed(ActionEvent e){
                                    
                                }
                            };
                            
                            boton.addActionListener(listener);
                            modelProductos.addRow(fila);
                        }
                       tblProductos.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

                         tblProductos.getTableHeader().setReorderingAllowed(true);
                         tblProductos.setPreferredScrollableViewportSize(new Dimension(500,80));
                         TableRowSorter<TableModel> ordena = new TableRowSorter<TableModel>(modelProductos);
                         tblProductos.setRowSorter(ordena);
                         tblProductos.setModel(modelProductos);
                         tblProductos.getColumnModel().getColumn(0).setPreferredWidth(20);
                         tblProductos.getColumnModel().getColumn(1).setPreferredWidth(130);
                         tblProductos.getColumnModel().getColumn(2).setPreferredWidth(130);
                         tblProductos.getColumnModel().getColumn(3).setPreferredWidth(130);
                         tblProductos.getColumnModel().getColumn(4).setPreferredWidth(130);
                    }
                }
                catch(Exception ex){}
                break;
            case "SEND_RESULT":
                break;
                
            case "GETSELECTEDROW":
                productoResultante.setIdProducto(Integer.parseInt(tblProductos.getValueAt(tblProductos.getSelectedRow(), 0).toString()));
                this.setVisible(false);
                break;
                
        }
    }
    
    
    void init(){
         try{
                    Services.VentaService n = new Services.VentaService();
                  
                        ArrayList productos = n.buscarProductos(new BO.ProductoBO(0, "" , "", null,0,0,0,0));                   
                        
                        Object[] fila = new Object[5];
                        
                        DefaultTableModel modelProductos = new DefaultTableModel();
                        modelProductos.addColumn("Id");
                        modelProductos.addColumn("Nombre");
                        modelProductos.addColumn("Precio");
                        modelProductos.addColumn("Precio a crédito");
                        modelProductos.addColumn("Clave");
                        
                        for(int i = 0; i < productos.size(); i ++){
                            fila[0] = Integer.toString(((ProductoBO)productos.get(i)).getIdProducto());
                            fila[1] = ((ProductoBO)productos.get(i)).getNombre();
                            fila[2] = Double.toString(((ProductoBO)productos.get(i)).getPrecio());
                            fila[3] = Double.toString(((ProductoBO)productos.get(i)).getPrecioCredito());
                            fila[4] = ((ProductoBO)productos.get(i)).getNombreClave();
                            modelProductos.addRow(fila);
                        }
                         tblProductos.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
                         tblProductos.getTableHeader().setReorderingAllowed(true);
                         TableRowSorter<TableModel> ordena = new TableRowSorter<TableModel>(modelProductos);
                         tblProductos.setRowSorter(ordena);
                         tblProductos.setModel(modelProductos);
                         tblProductos.getColumnModel().getColumn(0).setPreferredWidth(20);
                         tblProductos.getColumnModel().getColumn(1).setPreferredWidth(130);
                         tblProductos.getColumnModel().getColumn(2).setPreferredWidth(130);
                         tblProductos.getColumnModel().getColumn(3).setPreferredWidth(130);
                         tblProductos.getColumnModel().getColumn(4).setPreferredWidth(130);
                     
                         
                    
         
                    
                    
                }
                catch(Exception ex){}
                
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        btnBuscar = new javax.swing.JButton();
        txtClave = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblProductos = new javax.swing.JTable();
        btnAgregar = new javax.swing.JButton();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        setResizable(false);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Búsqueda de productos");

        btnBuscar.setText("Buscar");
        btnBuscar.setActionCommand("FIND");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Clave:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setText("Nombre:");

        tblProductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblProductos.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        tblProductos.setColumnSelectionAllowed(false);
        jScrollPane2.setViewportView(tblProductos);

        btnAgregar.setText("AGREGAR");
        btnAgregar.setActionCommand("GETSELECTEDROW");
        btnAgregar.setEnabled(false);
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(txtClave, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnBuscar))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(141, 141, 141)
                        .addComponent(jLabel2)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(147, 147, 147)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBuscar)
                    .addComponent(txtClave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel3)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addComponent(btnAgregar)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        try {
            ActionFacade(evt.getActionCommand().toString());
        } catch (ParseException ex) {
            Logger.getLogger(BusquedaPersonalizadaGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
        // TODO add your handling code here:
        
        try {
            ActionFacade(evt.getActionCommand().toString());
        } catch (ParseException ex) {
            Logger.getLogger(BusquedaPersonalizadaGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnAgregarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BusquedaPersonalizadaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BusquedaPersonalizadaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BusquedaPersonalizadaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BusquedaPersonalizadaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable tblProductos;
    private javax.swing.JTextField txtClave;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}
