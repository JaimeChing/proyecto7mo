/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import Fabrica.Conexion;
import Fabrica.DBFactory;
import BO.clientesBO;
import Fabrica.ConfigConexion;
import java.sql.SQLException;
/**
 *
 * @author AlexisENP
 */
public class ClienteQueryHlp {
    public Conexion conn()
    {
        ConfigConexion oConfigConexion=new ConfigConexion();       
        return oConfigConexion.conn();
    }
    public List<clientesBO> buscar(Object obj)
    {
        List<clientesBO> oListaCliente=new ArrayList<clientesBO>();
        try
            {
            String cadenawhere="";
            boolean edo=false;
            clientesBO data=(clientesBO)obj;
            Statement stml=conn().getConexion().createStatement();

            if(data.getIdClientes()>0){
               cadenawhere=cadenawhere +" Id_Clientes= "+ data.getIdClientes()+ " and";
               edo=true;
            }
            if(data.getNombre()!=null){
                cadenawhere=cadenawhere +" Nombres= '"+ data.getNombre() + "' and";
                edo=true;
            }
            if(data.getApellidosP()!=null){
                cadenawhere=cadenawhere +" ApellidoPaterno= '"+ data.getApellidosP()+ "' and";
                edo=true;
            }
            if(data.getApellidosM()!=null){
                cadenawhere=cadenawhere +" ApellidoMaterno= '"+ data.getApellidosM()+ "' and";
                edo=true;
            }
            if(data.getTelefono()!=null){
                cadenawhere=cadenawhere +" Telefono= '"+ data.getTelefono()+ "' and";
                edo=true;
            }
            if(data.getMail()!=null){
                cadenawhere=cadenawhere +" CorreoElectronico= '"+ data.getMail()+ "' and";
                edo=true;
            }
            if(data.getDireccion()!=null){
                cadenawhere=cadenawhere +" Direccion= '"+ data.getDireccion()+ "' and";
                edo=true;
            }
            if(data.getCredito()>0){
                cadenawhere=cadenawhere +" Credito= '"+ data.getCredito()+ "' and";
                edo=true;
            }
             if(data.getDeuda()!=null){
                cadenawhere=cadenawhere +" Deuda= '"+ data.getDeuda()+ "' and";
                edo=true;
            }
            if(edo==true){
                cadenawhere="where "+ cadenawhere.substring(0,cadenawhere.length()-3);
            }
                //-----
            ResultSet res=stml.executeQuery("Select * from clientes "+ cadenawhere);
            conn().cerrarConexion();
            while(res.next())
            {
                clientesBO oCliente=new clientesBO();
                oCliente.setIdClientes(Integer.parseInt(res.getString("Id_Clientes").toString()));
                oCliente.setNombre(res.getString("Nombres").toString());
                oCliente.setApellidosP(res.getString("ApellidoPaterno").toString());
                oCliente.setApellidosM(res.getString("ApellidoMaterno").toString());
                oCliente.setMail(res.getString("CorreoElectronico").toString());
                oCliente.setTelefono(res.getString("Telefono").toString());
                oCliente.setDireccion(res.getString("Direccion").toString());
                oCliente.setCredito(Integer.parseInt(res.getString("Credito").toString()));
                oCliente.setDeuda(res.getString("Deuda").toString());
                oListaCliente.add(oCliente);
            }

        }
        catch(SQLException ex)
        {
            conn().cerrarConexion();
            System.out.println(ex);
        }
        return oListaCliente;
    }

}
