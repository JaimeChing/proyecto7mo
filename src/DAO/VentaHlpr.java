/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;
import BO.*;
import Fabrica.Conexion;
import Fabrica.ConfigConexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Jaime
 */
public class VentaHlpr {
    
    public Conexion conn(){
        ConfigConexion oConfigConexion = new ConfigConexion();
        return oConfigConexion.conn();
    }
    
    
    public ProductoBO buscarProducto(int codigo) throws SQLException{
        ProductoBO oProducto= new ProductoBO();
        Statement stml=conn().getConexion().createStatement();
        ResultSet res=stml.executeQuery("Select * from Productos where IdProducto=" + Integer.toString(codigo));
        conn().cerrarConexion();
        while(res.next())
        {
            oProducto.setIdProducto(Integer.parseInt(res.getString("IdProducto")));
            oProducto.setNombre(res.getString("Nombre"));
            oProducto.setPrecio(Double.parseDouble(res.getString("Precio")));
            oProducto.setPrecioCredito(Double.parseDouble(res.getString("PrecioCredito")));
            oProducto.setStock(Integer.parseInt(res.getString("Stock")));
            oProducto.setNombreClave(res.getString("NombreClave"));
        }
        
        return oProducto;
    }
    
     public ArrayList buscarProductos(ProductoBO data){
        
         ArrayList<ProductoBO> n = new ArrayList();
         try
         {
             String cadenawhere="";
             boolean edo=false;
             Statement stml=conn().getConexion().createStatement();
            if(data.getIdProducto()>0){
               cadenawhere=cadenawhere +" IdProducto= "+ data.getIdProducto()+ " and";
               edo=true;
            }
            if(data.getNombre()!=null && !"".equals(data.getNombre())){
                cadenawhere=cadenawhere +" Nombre= '"+ data.getNombre() + "' and";
                edo=true;
            }
            
            if(data.getNombreClave() != null && !"".equals(data.getNombreClave())){
                cadenawhere=cadenawhere +" NombreClave= '"+ data.getNombreClave()+ "' and";
                edo=true;
            }
            
            if(data.getPrecio() > 0){
                cadenawhere=cadenawhere +" Precio= '"+ data.getPrecio()+ "' and";
                edo=true;
            }
            
             if(data.getStock()>0){
                cadenawhere=cadenawhere +" Stock= '"+ data.getStock()+ "' and";
                edo=true;
            }
            
            
            if(edo==true){
                cadenawhere="where "+ cadenawhere.substring(0,cadenawhere.length()-3);
            }
               
            ResultSet res=stml.executeQuery("Select * from Productos "+ cadenawhere);
            conn().cerrarConexion();
            while(res.next())
            {
                ProductoBO oCliente= new ProductoBO();
                oCliente.setIdProducto(Integer.parseInt(res.getString("IdProducto")));
                oCliente.setNombre(res.getString("Nombre"));
                oCliente.setPrecio(Double.parseDouble(res.getString("Precio")));
                oCliente.setPrecioCredito(Double.parseDouble(res.getString("PrecioCredito")));
                oCliente.setStock(Integer.parseInt(res.getString("Stock")));
                oCliente.setNombreClave(res.getString("NombreClave"));
                n.add(oCliente);
            }

        }
        catch(SQLException ex)
        {
            conn().cerrarConexion();
            System.out.println(ex);
        }
         return n;
    }
    
    public int generarVenta(VentaBO venta){
        int resultado =0;
        try{
            String query = "Insert into Ventas(Fecha,TotalCompra,Id_Clientes,IdUsuario,Tipo) Values(?,?,?,?,?);";
            PreparedStatement preparedStmt = conn().getConexion().prepareStatement(query);
            preparedStmt.setString(1, venta.getDate());
            preparedStmt.setDouble(2,venta.getTotalCompra());
            preparedStmt.setInt(3, venta.getIdCliente());
            preparedStmt.setInt(4, venta.getIdUsuario());
            preparedStmt.setString(5, venta.getTipo());
            boolean result = preparedStmt.execute();
            conn().cerrarConexion();
            if(!result){
                resultado=1;
                //Recuperar folio
                Statement stml=conn().getConexion().createStatement();
                query = "select max(IdVenta) as id from Ventas;";
                ResultSet res=stml.executeQuery(query);
                conn().cerrarConexion();
                int folio=0;
                while(res.next())
                {
                    folio = res.getInt("id");
                }
                try{
                    for(int i =0; i < venta.getN().size(); i++){
                        query = "Insert into ProductosVentas(Cantidad,IdProducto,IdVenta) values(?,?,?)";
                        preparedStmt = conn().getConexion().prepareStatement(query);
                        preparedStmt.setInt(1, venta.getN().get(i).getCantidadAVender());
                        preparedStmt.setInt(2,venta.getN().get(i).getIdProducto());
                        preparedStmt.setInt(3, folio);
                        result = preparedStmt.execute();
                        conn().cerrarConexion();
                        System.out.println("Producto agregado, folio " + folio);
                        
                        query = "Update  Productos set Stock=? Where IdProducto=" + venta.getN().get(i).getIdProducto();
                        preparedStmt = conn().getConexion().prepareStatement(query);
                        int helper = venta.getN().get(i).getStock() - venta.getN().get(i).getCantidadAVender();
                        preparedStmt.setInt(1, helper);
                        result = preparedStmt.execute();
                        conn().cerrarConexion();
                        System.out.println("Stock cambiado, folio " + folio);
                        
                        if("Crédito".equals(venta.getTipo())){
                            //Primero trae cliente
                            
                            ClienteBO cliente = traerCliente(venta.getIdCliente());
                            Double helperCredito = cliente.getCredito() - venta.getTotalCompra();
                            Double helperDeuda = cliente.getDeuda() + venta.getTotalCompra();
                            query = "Update  Clientes set Credito=?, Deuda=? Where Id_Clientes=" + venta.getIdCliente();
                            preparedStmt = conn().getConexion().prepareStatement(query);
                            preparedStmt.setDouble(1, helperCredito);
                            preparedStmt.setDouble(2, helperDeuda);
                            result = preparedStmt.execute();
                            conn().cerrarConexion();
                            System.out.println("Compra a crédito, folio " + folio);
                        }
                        
                    }
                }
                catch(Exception ex){
                    System.out.println(ex.toString());
                    conn().cerrarConexion();
                    resultado=0;
                }
            }
        }
        catch(SQLException ex)
        {
            conn().cerrarConexion();
            System.out.println(ex);
        }
        return resultado;
    }
    
    
    public ArrayList traerVentas(){
       ArrayList n = new ArrayList();
       
        try
         {
             Statement stml=conn().getConexion().createStatement();
             ResultSet res=stml.executeQuery("Select * from Ventas ");
             conn().cerrarConexion();
             while(res.next())
             {
                 VentaBO oVenta= new VentaBO();
                 oVenta.setIdCliente(Integer.parseInt(res.getString("Id_Clientes")));
                 oVenta.setIdVenta(Integer.parseInt(res.getString("IdVenta")));
                 oVenta.setIdUsuario(Integer.parseInt(res.getString("IdUsuario")));
                 oVenta.setDate(res.getString("Fecha"));
                 oVenta.setTotalCompra(Double.parseDouble(res.getString("TotalCompra")));
                 oVenta.setTipo(res.getString("Tipo"));
                 stml=conn().getConexion().createStatement();
                 ResultSet helper =stml.executeQuery("Select pv.Cantidad, pv.IdProductoVenta as Folio, pv.IdVenta, p.Nombre as NombreP, p.Precio, p.PrecioCredito, p.NombreClave, p.Stock, p.IdProducto from productosventas pv join Productos p on p.IdProducto=pv.IdProducto where IdVenta=" + oVenta.getIdVenta());
                 conn().cerrarConexion();
                 ArrayList productos = new ArrayList();
                 while(helper.next()){
                     ProductoBO oProducto = new ProductoBO();
                     oProducto.setIdProducto(Integer.parseInt(helper.getString("IdProducto")));
                     oProducto.setNombre(helper.getString("NombreP"));
                     oProducto.setNombreClave("NombreClave");
                     oProducto.setPrecio(Double.parseDouble(helper.getString("Precio")));
                     oProducto.setPrecioCredito(Double.parseDouble(helper.getString("PrecioCredito")));
                     oProducto.setStock(Integer.parseInt(helper.getString("Stock")));
                     oProducto.setCantidadAVender(Integer.parseInt(helper.getString("Cantidad")));
                     productos.add(oProducto);
                 }
                 oVenta.setN(n);
                 n.add(oVenta);
             }
        }
        catch(SQLException ex)
        {
            conn().cerrarConexion();
            System.out.println(ex);
        }
       
       
       return n;
       
    }
    
    public ArrayList traerClientes (){
        ArrayList n = new ArrayList();
         try
         {
             Statement stml=conn().getConexion().createStatement();
             ResultSet res=stml.executeQuery("Select * from Clientes ");
             conn().cerrarConexion();
             while(res.next())
             {
                 ClienteBO oCliente= new ClienteBO();
                 oCliente.setNombres(res.getString("Nombres"));
                 oCliente.setId_cliente(Integer.parseInt(res.getString("Id_Clientes")));
                 oCliente.setApellido_Materno(res.getString("ApellidoMaterno"));
                 oCliente.setApellido_Paterno(res.getString("ApellidoPaterno"));
                 oCliente.setTelefono(res.getString("Telefono"));
                 oCliente.setDireccion(res.getString("Direccion"));
                 oCliente.setCorreo_electronico(res.getString("CorreoElectronico"));
                 oCliente.setCredito(Double.parseDouble(res.getString("Credito")));
                 oCliente.setDeuda(Double.parseDouble(res.getString("Deuda")));
                 n.add(oCliente);
             }
        }
        catch(SQLException ex)
        {
            conn().cerrarConexion();
            System.out.println(ex);
        }
        return n;
    }
 
    public ClienteBO traerCliente( int idCliente){
        ClienteBO oCliente= new ClienteBO();
         try
         {
             Statement stml=conn().getConexion().createStatement();
             ResultSet res=stml.executeQuery("Select * from Clientes where Id_Clientes=" + idCliente);
             conn().cerrarConexion();
             while(res.next())
             {
                 oCliente.setNombres(res.getString("Nombres"));
                 oCliente.setId_cliente(Integer.parseInt(res.getString("Id_Clientes")));
                 oCliente.setApellido_Materno(res.getString("ApellidoMaterno"));
                 oCliente.setApellido_Paterno(res.getString("ApellidoPaterno"));
                 oCliente.setTelefono(res.getString("Telefono"));
                 oCliente.setDireccion(res.getString("Direccion"));
                 oCliente.setCorreo_electronico(res.getString("CorreoElectronico"));
                 oCliente.setCredito(Double.parseDouble(res.getString("Credito")));
                 oCliente.setDeuda(Double.parseDouble(res.getString("Deuda")));
             }

        }
        catch(SQLException ex)
        {
            conn().cerrarConexion();
            System.out.println(ex);
        }
        return oCliente;
    }    
    
    
    
    
    
}
