/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fabrica;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 *
 * @author Eduardo
 */
public class ConexionMySQL extends Conexion{

            @Override
    	public   String establecerConexion(){
            //String url="jdbc:mysql://servidor7c.dynu.net:3306/proyecto7c";
            //String url = "jdbc:mysql://bd7c.cle7b9n80rkf.us-east-1.rds.amazonaws.com:3306/proyecto7c";
            String url = "jdbc:mysql://localhost:3306/proyecto7c";
            //String url = "jdbc:mysql://servidor7c.dynu.net:3306/proyecto7c";
            return  url;
        }
            @Override
	public  Connection getConexion(){
            try{
			Class.forName("com.mysql.jdbc.Driver");
			conn = (Connection) DriverManager.getConnection(establecerConexion(), "root", "Ronquillo#2");
			if(conn != null){
				System.out.println("Se ha establecido la conexión.");
			}
			else{
				System.out.println("Se ha denegado la conexión.");
			}
		}
		catch(SQLException e){
				System.out.println(e);
		}
		catch(ClassNotFoundException e){
	
			System.out.println(e);
		}
		return conn;
        }
            @Override
        public  void cerrarConexion(){
            conn=null;
        }
}
