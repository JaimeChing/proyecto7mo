/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fabrica;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 *
 * @author Jaime
 */
public abstract class Conexion {
        Connection conn=null;
	public  abstract String establecerConexion();
	public abstract Connection getConexion();
        public abstract void cerrarConexion();
}
