/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fabrica;

/**
 *
 * @author Eduardo
 */
public class DBFactory implements DBFactoryMethod {
public DBFactory(){}
 public  Conexion CreaConexion(String tipo)
 {
     switch (tipo) {
	case "SqlServer":
        {
            ConexionSQLServer oCxSQLServer= new ConexionSQLServer();
            return oCxSQLServer; 
        }	
	case "Mysql":
        {	
            ConexionMySQL oCxMySQL=new ConexionMySQL();
            return oCxMySQL;
        }
	
	default:
		return null;
	}
 }
}
