/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import java.util.Date;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
/**
 *
 * @author Andre
 */
public class UsuarioBO {
    Integer idUsuario;
    String nombreUsuario;
    String apellidoUsuario;
    String telefonoUsuario;
    Date fechaNacimiento;
    String direccionUsuario;
    String rolUsuario;
    static UsuarioBO empleado;
    
    private UsuarioBO(){    
    }
    
    public void setId(Integer id){
        this.idUsuario = id;
    }
    
    public Integer getId(){
        return this.idUsuario;
    }
    
    public void setNombre(String nombre){
        this.nombreUsuario = nombre;
    }
    
    public String getNombre(){
        return this.nombreUsuario;
    }
    
    public void setApellido(String apellido){
        this.apellidoUsuario = apellido;
    }
    
    public String getApellido(){
        return this.apellidoUsuario;
    }
    
    public void setTelefono(String telefono){
        this.telefonoUsuario = telefono;
    }
    
    public String getTelefono(){
        return this.telefonoUsuario;
    }
    
    public void setFechaNacimiento(Date fechaNacimiento){
        this.fechaNacimiento = fechaNacimiento;
    }
    
    public Date getFechaNacimiento(){
        return this.fechaNacimiento;
    }
    
    public void setDireccion(String direccion){
        this.direccionUsuario = direccion;
    }
    
    public String getDireccion(){
        return this.direccionUsuario;
    }
    
    public void setRol(String rol){
        this.rolUsuario = rol;
    }
    
    public String getRol(){
        return this.rolUsuario;
    }
    
    public UsuarioBO getInstanciaEmpleado(){
        if(empleado == null){
            empleado = new UsuarioBO();
            /*empleado.nombreUsuario = "Oscar";
            empleado.apellidoUsuario = "Vera";
            try{
            empleado.fechaNacimiento = (new SimpleDateFormat("dd/MM/yyyy")).parse("1996-01-10");
            } catch (Exception e) { empleado.fechaNacimiento = new Date(); }
            empleado.telefonoUsuario = "9992142371";
            empleado.rolUsuario = "Empleado";*/
        }
        return empleado;
    }
}
