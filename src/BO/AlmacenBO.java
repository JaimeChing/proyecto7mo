/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

/**
 *
 * @author carlo
 */
public class AlmacenBO {
    int IdAlmacen;
    String NombreAlmacen;
    String DireccionAlmacen;
    String CodigoPostalAlmacen;

    public int getIdAlmacen() {
        return IdAlmacen;
    }

    public void setIdAlmacen(int IdAlmacen) {
        this.IdAlmacen = IdAlmacen;
    }

    public String getNombreAlmacen() {
        return NombreAlmacen;
    }

    public void setNombreAlmacen(String NombreAlmacen) {
        this.NombreAlmacen = NombreAlmacen;
    }

    public String getDireccionAlmacen() {
        return DireccionAlmacen;
    }

    public void setDireccionAlmacen(String DireccionAlmacen) {
        this.DireccionAlmacen = DireccionAlmacen;
    }

    public String getCodigoPostalAlmacen() {
        return CodigoPostalAlmacen;
    }

    public void setCodigoPostalAlmacen(String CodigoPostalAlmacen) {
        this.CodigoPostalAlmacen = CodigoPostalAlmacen;
    }
    
    
    
}
