/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import java.util.ArrayList;

/**
 *
 * @author Jaime
 */
public class VentaBO {

    public int getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(int idVenta) {
        this.idVenta = idVenta;
    }

    public double getTotalCompra() {
        return totalCompra;
    }

    public void setTotalCompra(double totalCompra) {
        this.totalCompra = totalCompra;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public ArrayList<ProductoBO> getN() {
        return n;
    }

    public void setN(ArrayList<ProductoBO> n) {
        this.n = n;
    }

    public VentaBO(int idVenta, double totalCompra, int idCliente, int idUsuario, ArrayList<ProductoBO> n) {
        this.idVenta = idVenta;
        this.totalCompra = totalCompra;
        this.idCliente = idCliente;
        this.idUsuario = idUsuario;
        this.n = n;
    }
    
    public VentaBO(){
        
    }
    String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    String tipo;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    int idVenta;
    double totalCompra;
    int idCliente;
    int idUsuario;
    ArrayList<ProductoBO> n;
    int[] idProductos;

    public int[] getIdProductos() {
        return idProductos;
    }

    public void setIdProductos(int[] idProductos) {
        this.idProductos = idProductos;
    }
    
    
    
    
    
}
