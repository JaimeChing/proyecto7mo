/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

/**
 *
 * @author Jaime
 */
public class ProductoBO {
    
    
     public ProductoBO(int idProducto, String nombreClave, String nombre, String descripcion, double precio, int stock, double precioCredito, int idCategoria) {
        this.idProducto = idProducto;
        this.nombreClave = nombreClave;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio = precio;
        this.stock = stock;
        this.precioCredito = precioCredito;
        this.idCategoria = idCategoria;
    }

    public ProductoBO() {
    }
    
    
    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombreClave() {
        return nombreClave;
    }

    public void setNombreClave(String nombreClave) {
        this.nombreClave = nombreClave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public double getPrecioCredito() {
        return precioCredito;
    }

    public void setPrecioCredito(double precioCredito) {
        this.precioCredito = precioCredito;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

   
    int idProducto;
    String nombreClave;
    String nombre;
    String descripcion;
    double precio;
    int stock;
    double precioCredito;
    int idCategoria;
    int cantidadAVender;

    public int getCantidadAVender() {
        return cantidadAVender;
    }

    public void setCantidadAVender(int cantidadAVender) {
        this.cantidadAVender = cantidadAVender;
    }
    
    
}
